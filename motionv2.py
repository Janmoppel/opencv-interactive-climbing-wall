import time
import cv2
import numpy
import random
import copy

# global variables
min_area=50
camera = cv2.VideoCapture(0)
key=ord("1")
width=1024
height=768
ballx=random.randint(100,width-100)
bally=random.randint(100,height-100)
counter=0

# initialize the first frame in the video stream
firstFrame = None
firstFrame_original = None

# mainloop
while True:
	# grab the current frame
	(grabbed, frame) = camera.read()

	# resize the frame, convert it to grayscale, and blur it
	frame = cv2.resize(frame,(width,height),fx=1,fy=1)
	frame = cv2.flip(frame, flipCode = 1)
	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	gray2 = gray
	frame2 = frame

	gray = cv2.GaussianBlur(gray, (21, 21), 0)
 	mask = numpy.zeros((height, width, 3), numpy.uint8)
        cv2.circle(mask, (ballx,bally),20, (255,255,255), thickness=-1, lineType=8, shift=0)

	mask=cv2.cvtColor(mask,cv2.COLOR_BGR2GRAY)
	gray_original=gray
        gray=gray*mask

	# if the first frame is None, initialize it
	if firstFrame is None:
		firstFrame = gray
		continue
	if firstFrame_original is None:
		firstFrame_original=gray_original
		continue
# compute the absolute difference between the current frame and
	# first frame
	frameDelta = cv2.absdiff(firstFrame, gray)
	thresh = cv2.threshold(frameDelta, 25, 255, cv2.THRESH_BINARY)[1]

	# dilate the thresholded image to fill in holes, then find contours
	# on thresholded image
	thresh = cv2.dilate(thresh, None, iterations=2)
	(cnts, _) = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_SIMPLE)

#frame delta for display purposes
        frameDelta_display = cv2.absdiff(firstFrame_original, gray_original)
        thresh_display = cv2.threshold(frameDelta_display, 25, 255, cv2.THRESH_BINARY)[1]
        thresh_display = cv2.dilate(thresh_display, None, iterations=2)

	# loop over the contours
	for c in cnts:
		# if the contour is too small, ignore it
		if cv2.contourArea(c) < min_area:
			continue

		# compute the bounding box for the contour, draw it on the frame,
		# and update the first frame
		ballx=random.randint(100,width-100)
		bally=random.randint(100,height-100)
		firstFrame=None
		firstFrame_original=None
		counter+=1
		
	# show the frame
	mask_inv = cv2.bitwise_not(mask)
	frame = cv2.bitwise_and(frame,frame,mask=mask_inv)
	
#	cv2.putText(frame, "Room Status: {}".format(text), (10, 20),cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
	if counter==1:
		start=time.time()
	if counter == 0:
		stop_watch="00:00"
	if counter!=0 and counter<=4:
		stop_watch=str(round(time.time()-start,2))
		
	cv2.putText(frame,"timer: "+stop_watch,(100,height-80),cv2.FONT_HERSHEY_SIMPLEX,1,(0,0,0),1)
	if key==ord("1"):
#	        cv2.circle(frame, (herp,derp),20, (255,255,0), thickness=-1, lineType=8, shift=0)
        	cv2.imshow("Window", frame)

	if key==ord("2"):
        	cv2.putText(mask,"MASK",(100,height-80),cv2.FONT_HERSHEY_SIMPLEX,1,(255,255,255),1)
	        cv2.imshow("Window", mask)

        if key==ord("3"):
        	cv2.putText(thresh_display,"THRESHOLD",(100,height-80),cv2.FONT_HERSHEY_SIMPLEX,1,(255,255,255),1)
                cv2.imshow("Window", thresh_display)

        if key==ord("4"):
	        cv2.putText(frameDelta_display,"FRAME DELTA",(100,height-80),cv2.FONT_HERSHEY_SIMPLEX,1,(255,255,255),1)
                cv2.imshow("Window", frameDelta_display)

	if key==ord("5"):
	        cv2.putText(gray2,"GRAYSCALE",(100,height-80),cv2.FONT_HERSHEY_SIMPLEX,1,(255,255,255),1)
		cv2.imshow("Window", gray2)

	if key==ord("6"):
	        cv2.putText(frame2,"RAW INPUT",(100,height-80),cv2.FONT_HERSHEY_SIMPLEX,1,(255,255,255),1)
		cv2.imshow("Window",frame2)

	key_helper = cv2.waitKey(1) & 0xFF
	if key_helper==ord("q") or key_helper==ord("6") or key_helper==ord("5") or key_helper==ord("1") or key_helper==ord("2") or key_helper==ord("3") or key_helper==ord("4") :
		key=key_helper
	# if the `q` key is pressed, break from the lop
	if key == ord("q"):
		break

# cleanup the camera and close any open windows
camera.release()
cv2.destroyAllWindows()


