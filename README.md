# README #

This repository is for the interactive climbing wall project. It is based on Python 2.7 and uses the OpenCV2 library.

The main part of the project is motionv2.py, which performs motion detection and basic timing mechanics. Other files in the repository
are side projects in earlier phases.