# import the necessary packages
#import argparse
#import datetime
#import time
import cv2
import numpy
import random
# construct the argument parser and parse the arguments
#ap = argparse.ArgumentParser()
#ap.add_argument("-v", "--video", help="path to the video file")
#ap.add_argument("-a", "--min-area", type=int, default=500, help="minimum area size")
#args = vars(ap.parse_args())
 
# if the video argument is None, then we are reading from webcam
camera = cv2.VideoCapture(0)
#cv2.flip(camera, camera, flipMode=1)
time.sleep(0.25)
 
# initialize the first frame in the video stream
firstFrame = None
wat=random.randint(100,400)
wat2=wat+30


original_ball = cv2.imread("pall.png",)
ball = cv2.resize(original_ball, (30,30), fx=1, fy=1)
mask_original = cv2.imread("mask.png",)
mask=cv2.resize(mask_original, (30,30)	, fx=1, fy=1)

# loop over the frames of the video
while True:
	# grab the current frame and initialize the occupied/unoccupied
	# text
	(grabbed, frame) = camera.read()
#	text = "no trigger"
	#cv2.flip(frame, frame, flipMode=1)
	# if the frame could not be grabbed, then we have reached the end
	# of the video
	if not grabbed:
		break
 
	# resize the frame, convert it to grayscale, and blur it
	#frame = imutils.resize(frame, width=500)
	frame= cv2.resize(frame,(500,375),fx=1,fy=1)
	frame = cv2.flip(frame, flipCode = 1)
	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	gray = cv2.GaussianBlur(gray, (21, 21), 0)
 	image = numpy.zeros((375, 500, 3), numpy.uint8)
        image[:]=(0,0,0)
        image[wat:wat2, wat:wat2]=mask
	image=cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
        gray=gray*image

	# if the first frame is None, initialize it
	if firstFrame is None:
		firstFrame = gray
		continue
# compute the absolute difference between the current frame and
	# first frame
	frameDelta = cv2.absdiff(firstFrame, gray)
	thresh = cv2.threshold(frameDelta, 25, 255, cv2.THRESH_BINARY)[1]
 
	# dilate the thresholded image to fill in holes, then find contours
	# on thresholded image
	thresh = cv2.dilate(thresh, None, iterations=2)
	(cnts, _) = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_SIMPLE)
 
	# loop over the contours
	for c in cnts:
		# if the contour is too small, ignore it
		if cv2.contourArea(c) < args["min_area"]:
			continue

		# compute the bounding box for the contour, draw it on the frame,
		# and update the text
		(x, y, w, h) = cv2.boundingRect(c)
		#cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
		text = "trigger"
		wat=random.randint(100,300)
		wat2=wat+30
		firstFrame=None
# draw the text and timestamp on the frame
#	cv2.putText(frame, "Status: {}".format(text), (10, 20),
#		cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
	#cv2.putText(frame, datetime.datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
	#	(10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)

	# show the frame and record if the user presses a key
	#frame[wat:wat2,wat:wat2]=ball

	mask_inv = cv2.bitwise_not(image)
	frame = cv2.bitwise_and(frame,frame,mask = mask_inv)
	cv2.imshow("Security Feed", frame)
#	cv2.imshow("Security Feed", ball)
	#cv2.imshow("Thresh", thresh)
	#cv2.imshow("Frame Delta", frameDelta)
	key = cv2.waitKey(1) & 0xFF

	# if the `q` key is pressed, break from the lop
	if key == ord("q"):
		break

# cleanup the camera and close any open windows
camera.release()
cv2.destroyAllWindows()


