import time
import cv2
import numpy
import random
# global variables
min_area=50
camera = cv2.VideoCapture(0)
#time.sleep(0.25)
key=ord("1")
width=1200
height=900
ballx=width
bally=0
counter=0
herp=width+100
derp=height+100
delay=30

# initialize the first frame in the video stream
firstFrame = None
firstFrame_original = None
# mainloop
while True:
	# grab the current frame
	(grabbed, frame) = camera.read()

	# resize the frame, convert it to grayscale, and blur it
	frame = cv2.resize(frame,(width,height),fx=1,fy=1)
	frame = cv2.flip(frame, flipCode = 1)
	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	gray = cv2.GaussianBlur(gray, (21, 21), 0)
 	mask = numpy.zeros((height, width, 3), numpy.uint8)
        cv2.circle(mask, (ballx,bally),200, (255,255,255), thickness=-1, lineType=8, shift=0)

	mask=cv2.cvtColor(mask,cv2.COLOR_BGR2GRAY)
	gray_original=gray
        gray=gray*mask

	# if the first frame is None, initialize it
	if firstFrame is None:
		firstFrame = gray
		continue
	if firstFrame_original is None:
		firstFrame_original=gray_original
		continue
# compute the absolute difference between the current frame and
	# first frame
	frameDelta = cv2.absdiff(firstFrame, gray)
	thresh = cv2.threshold(frameDelta, 25, 255, cv2.THRESH_BINARY)[1]

	# dilate the thresholded image to fill in holes, then find contours
	# on thresholded image
	thresh = cv2.dilate(thresh, None, iterations=2)
	(cnts, _) = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_SIMPLE)

#frame delta for display purposes
        frameDelta_display = cv2.absdiff(firstFrame_original, gray_original)
        thresh_display = cv2.threshold(frameDelta_display, 25, 255, cv2.THRESH_BINARY)[1]
        thresh_display = cv2.dilate(thresh_display, None, iterations=2)
	# loop over the contours
	for c in cnts:
		# if the contour is too small, ignore it
		if cv2.contourArea(c) < min_area or delay>0 or herp>width or herp<width-130:
			continue

		# compute the bounding box for the contour, draw it on the frame,
		# and update the first frame
#		ballx=random.randint(100,width-100)
#		bally=random.randint(100,height-100)
		firstFrame=None
		firstFrame_original=None
		counter+=1
		delay=30
		print("trigger")

	delay-=1
#	if key_helper == ord("n"):
#		herp=0
	# show the frame
	mask_inv = cv2.bitwise_not(mask)
#	frame = cv2.bitwise_and(frame,frame,mask=mask_inv)
#	cv2.putText(frame, "Room Status: {}".format(text), (10, 20),cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
#	if counter==1:
#		start=time.time()
#	if counter == 0:
#		stop_watch="00:00"
#	if counter!=0 and counter<=4:
#		stop_watch=str(round(time.time()-start,2))
	cv2.putText(frame,"Trigger counter: "+str(counter),(100,height-80),cv2.FONT_HERSHEY_SIMPLEX,1,(0,0,0),1)
	if key==ord("1"):
	        cv2.circle(frame, (herp,derp),20, (255,255,0), thickness=-1, lineType=8, shift=0)
        	cv2.imshow("Window", frame)
		herp+=15
		derp-=11
	if key==ord("2"):
	        cv2.imshow("Window", mask)
        if key==ord("3"):
                cv2.imshow("Window", thresh_display)
        if key==ord("4"):
                cv2.imshow("Window", frameDelta_display)
	key_helper = cv2.waitKey(1) & 0xFF
        if key_helper == ord("n"):
                herp=width/2
		derp=height/2
	if key_helper==ord("q") or key_helper==ord("1") or key_helper==ord("2") or key_helper==ord("3") or key_helper==ord("4") :
		key=key_helper
	# if the `q` key is pressed, break from the lop
	if key == ord("q"):
		break

# cleanup the camera and close any open windows
camera.release()
cv2.destroyAllWindows()


